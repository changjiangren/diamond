package com.qlzj.diamond.dao;

import java.util.List;
import java.util.Date;
import java.util.Map;

import com.qlzj.diamond.model.TComment;
import org.apache.ibatis.annotations.Param;


/**
 * 
 * 评论表
 * @author menghaojie
 * @date 2018-12-02 21:38
 **/
public interface TCommentDao {

	public TComment selectByPrimaryKey(Long id);

	public int deleteByPrimaryKey(Long id);

	public int insertSelective(TComment tComment);

	public int updateByPrimaryKeySelective(TComment tComment);

	public Long selectObjectListPageTotal(TComment tComment);

	public List<TComment> selectObjectListPage(TComment tComment);

	public List<TComment> selectByObjectList(TComment tComment);

	public int deleteByCommentId(@Param("commentId") Long commentId);

}
