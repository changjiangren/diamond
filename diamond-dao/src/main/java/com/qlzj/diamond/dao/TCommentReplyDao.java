package com.qlzj.diamond.dao;

import java.util.List;
import java.util.Date;
import java.util.Map;

import com.qlzj.diamond.model.TCommentReply;
import org.apache.ibatis.annotations.Param;


/**
 * 
 * 回复表
 * @author menghaojie
 * @date 2018-12-02 21:38
 **/
public interface TCommentReplyDao {

	public TCommentReply selectByPrimaryKey(Long id);

	public int deleteByPrimaryKey(Long id);

	public int insertSelective(TCommentReply tCommentReply);

	public int updateByPrimaryKeySelective(TCommentReply tCommentReply);

	public Long selectObjectListPageTotal(TCommentReply tCommentReply);

	public List<TCommentReply> selectObjectListPage(TCommentReply tCommentReply);

	public List<TCommentReply> selectByObjectList(TCommentReply tCommentReply);

	public List<TCommentReply> selectByCommentId(@Param("commentId") Long commentId);

}
