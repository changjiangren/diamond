package com.qlzj.diamond.dao;

import java.util.List;
import java.util.Date;
import java.util.Map;

import com.qlzj.diamond.model.TQuestion;
import org.apache.ibatis.annotations.Param;


/**
 * 
 * 问题表
 * @author menghaojie
 * @date 2018-11-29 17:11
 **/
public interface TQuestionDao {

	public TQuestion selectByPrimaryKey(Long id);

	public int deleteByPrimaryKey(Long id);

	public int insertSelective(TQuestion tQuestion);

	public int updateByPrimaryKeySelective(TQuestion tQuestion);

	public Long selectObjectListPageTotal(TQuestion tQuestion);

	public List<TQuestion> selectObjectListPage(TQuestion tQuestion);

	public List<TQuestion> selectByObjectList(TQuestion tQuestion);

	int deleteQuestionByUserId(@Param("userId") Long userId, @Param("questionId") Long questionId);

	List<TQuestion> searchQuestionByUserId(@Param("userId") Long userId);

}
