package com.qlzj.diamond.dao;

import java.util.List;
import java.util.Date;
import java.util.Map;

import com.qlzj.diamond.model.Users;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;


/**
 * 
 * 
 * @author hailin.su
 * @date 2018-11-27 14:11
 **/
public interface UsersDao {

	public Users selectByPrimaryKey(Long id);

	public int deleteByPrimaryKey(Long id);

	public int insertSelective(Users users);

	public int updateByPrimaryKeySelective(Users users);

	public Long selectObjectListPageTotal(Users users);

	public List<Users> selectObjectListPage(Users users);

	public List<Users> selectByObjectList(Users users);

}
