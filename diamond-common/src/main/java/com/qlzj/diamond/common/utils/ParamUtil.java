package com.qlzj.diamond.common.utils;

import org.apache.commons.lang.math.NumberUtils;

public class ParamUtil {

    /**
     * 为空判断
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return str == null || str.length() <= 0;
    }

    /**
     * 不为空判断
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 验证
     * @param i
     * @return
     */
    public static boolean isPositive(Integer i) {
        if (i != null && i.compareTo(NumberUtils.INTEGER_ZERO) > 0) {
            return true;
        }
        return false;
    }

    public static boolean isPositive(Long l) {
        if (l != null && l.compareTo(NumberUtils.LONG_ZERO) > 0 ) {
            return true;
        }
        return false;
    }

    public static boolean isNaturalNumber(Integer i) {
        if(i != null && i.compareTo(NumberUtils.INTEGER_ZERO) >= 0) {
            return true;
        }
        return false;
    }

    public static boolean isNaturalNumber(Long l) {
        if(l != null && l.compareTo(NumberUtils.LONG_ZERO) >= 0) {
            return true;
        }
        return false;
    }
}
