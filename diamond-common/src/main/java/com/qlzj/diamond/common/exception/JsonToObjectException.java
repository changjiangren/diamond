package com.qlzj.diamond.common.exception;

/**
 * @Author menghaojie
 * 2018-11-29
 */
public class JsonToObjectException extends RuntimeException {
    private static final long serialVersionUID = 1418604259745317388L;

    public JsonToObjectException() {
    }

    public JsonToObjectException(String message, Throwable cause) {
        super(message, cause);
    }
}
