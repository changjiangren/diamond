package com.qlzj.diamond.common.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.qlzj.diamond.common.exception.JsonToObjectException;
import com.qlzj.diamond.common.exception.ObjectToJsonException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonUtils {

    public static final Logger LOGGER = LoggerFactory.getLogger(JsonUtils.class);

    private static ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
        objectMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        objectMapper.configure(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS, true);
        objectMapper.configure(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS, true);
        objectMapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
        objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        SimpleModule module = new SimpleModule("DateTimeModule", Version.unknownVersion());
        //null的字段不输出,减少数据量,也避免.NET系统用基本类型反序列化本系统的包装类型字段,导致出错
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.registerModule(module);
    }

    public static ObjectMapper getObjectMapperInstance() {
        return objectMapper;
    }

    /**
     * 使用Jackson序列化字符串,出异常后写日志但不抛异常
     * @return
     */
    public static String writeObjectAsStringSilently(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("Json to String error, return null", e);
            return null;
        }
    }

    /**
     * 使用Jackson序列化字符串,出异常后写日志&抛异常
     * @return
     */
    public static String writeObjectAsString(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("Jackson Convert Object to String error, return null", e);
            throw new ObjectToJsonException("Jackson转字符串时报错", e);
        }
    }

    /**
     * 将字符串转为Java对象
     * @param data
     * @param c
     * @param <T>
     * @return
     */
    public static <T> T readValue(String data, Class<T> c) {
        try {
            return objectMapper.readValue(data, c);
        } catch (Exception e) {
            LOGGER.error("Jackson Convert String to Object error, return null", e);
            throw new JsonToObjectException("Jackson转对象时报错", e);
        }
    }
}