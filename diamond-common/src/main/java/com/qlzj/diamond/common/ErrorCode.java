package com.qlzj.diamond.common;

import java.io.Serializable;

/**
 * @author haojiem@tujia.com
 * @date 2018/11/26
 */
public class ErrorCode implements Serializable {
    private static final long serialVersionUID = -4304547322046723876L;
    public ErrorCode() {

    }

    public ErrorCode(int code, String message) {
        super();
        this.code = code;
        this.message = message;
    }

    public ErrorCode(int code, String message, Object... param) {
        super();
        this.code = code;
        this.message = String.format(message,param);
        this.param = param;
    }

    private int code;
    private String message;
    private Object[] param;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object[] getParam() {
        return param;
    }

    public void setParam(Object[] param) {
        this.param = param;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ErrorCode [code=");
        builder.append(code);
        builder.append(", message=");
        builder.append(message);
        builder.append("]");
        return builder.toString();
    }
}
