package com.qlzj.diamond.common.utils;

import com.qlzj.diamond.model.TQuestion;

public class QuestionValiUtil {

    public static boolean baseValidate(TQuestion question){
        return ParamUtil.isPositive(question.getUserId())
                && ParamUtil.isNotEmpty(question.getQuesContent())
                && ParamUtil.isNotEmpty(question.getQuesTitle())
                && ParamUtil.isNaturalNumber(question.getPublicStatus())
                && ParamUtil.isNaturalNumber(question.getAnonymousStatus());
    }
}
