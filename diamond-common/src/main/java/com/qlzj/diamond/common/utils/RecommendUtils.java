package com.qlzj.diamond.common.utils;

import com.qlzj.diamond.model.recommend.Recommend;
import com.qlzj.diamond.model.recommend.SimilarytyArrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecommendUtils {

    public static List<Recommend> getRecommendList(List<SimilarytyArrays> sourceList){
        double distanceMy = 0.00;
        List<Recommend> targetList = new ArrayList<>();
        for (SimilarytyArrays  similarytyArrays : sourceList) {
            Recommend recommend = new Recommend();
            distanceMy = getDistanceMy(similarytyArrays.getUserInterest(), similarytyArrays.getTargetInterest(),
                    similarytyArrays.getUserWant(), similarytyArrays.getTargetCan());
            recommend.setDistance(distanceMy);
            recommend.setId(similarytyArrays.getTargetId());
            targetList.add(recommend);
        }
        Collections.sort(targetList);
        return targetList;
    }

    public static double getDistanceMy(long[] uI, long[] tL,long[] uW,long[] tC) {
        double iv = sameCount(uI, tL, 10);
        double wv = sameCount(uW, tC, 5);
        return iv + wv;
    }

    public static double sameCount(long[] i, long[] j,int weight){
        int sames = 0;
        for(int k=0;k<i.length;k++) {
            for (int l = 0; l < j.length; l++) {
                if (i[k] == j[l])
                    sames += 1;
            }
        }
        return (double) sames/i.length * weight;
    }
}
