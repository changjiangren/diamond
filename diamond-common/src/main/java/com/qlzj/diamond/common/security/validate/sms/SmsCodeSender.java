/**
 * 
 */
package com.qlzj.diamond.common.security.validate.sms;

public interface SmsCodeSender {
	
	/**
	 * @param mobile
	 * @param code
	 */
	void send(String mobile, String code);

}
