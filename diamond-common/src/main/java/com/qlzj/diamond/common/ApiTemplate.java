package com.qlzj.diamond.common;

import com.qlzj.diamond.common.exception.BizException;
import com.qlzj.diamond.model.APIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author haojiem@tujia.com
 * Api逻辑模板， 所有的对外暴露的服务的方法实现均需要使用这个模板来编写
 * @date 2018/11/24
 */
public abstract class ApiTemplate<T> {

    private static final Logger logger = LoggerFactory.getLogger(ApiTemplate.class);

    protected String serviceName;//服务名称

    protected ApiTemplate(String serviceName) {
        this.serviceName = serviceName;
    }

    protected abstract void checkParams() throws Exception;

    protected abstract APIResponse<T> process() throws Exception;

    protected void afterProcess() {}

    protected void onSuccess() {}
    

    protected APIResponse<T> onBizError(BizException biz) {
        logger.error("biz error while execute，params:{} error msg:{}", biz.getMessage(), biz);
        return APIResponse.returnFail(biz.getBizErrorCode(), biz.getMessage());
    }

    protected APIResponse<T> onError(Throwable e) {
        logger.error("unexpected error while execute，params:{}  error msg:{}", e.getMessage(), e);
        return APIResponse.returnFail(0, e.getMessage());
    }

    private APIResponse<T> defaultHandleError() {
        return APIResponse.returnFail("未知错误");
    }

    public APIResponse<T> execute() {
        long start = System.currentTimeMillis();
        try {
            // 1.check
            checkParams();

            // 2.handler
            APIResponse<T> result = process();

            // 3.after doing
            onSuccess();

            return result;
        } catch (BizException biz) {
            try {
                return onBizError(biz);
            } catch (Exception e) {
                logger.error("execute onBizError fail", e);
                return defaultHandleError();
            }
        } catch (Throwable e) {
            try {
                return onError(e);
            } catch (Exception t) {
                logger.error("execute onError fail", t);
                return defaultHandleError();
            }
        } finally {
            try {
                afterProcess();
            } catch (Exception e) {
                logger.error("execute afterProcess fail", e);
            }
        }
    }
}
