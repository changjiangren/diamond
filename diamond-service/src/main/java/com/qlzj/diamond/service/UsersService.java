package com.qlzj.diamond.service;

import java.util.List;

import com.qlzj.diamond.api.IUsersService;
import com.qlzj.diamond.model.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.qlzj.diamond.dao.UsersDao;


/**
 * 
 * 
 * @author hailin.su
 * @date 2018-11-27 14:11
 **/
@Service
public class UsersService implements IUsersService {

	private static Logger logger = LoggerFactory.getLogger(UsersService.class);

	@Resource
	private UsersDao usersDao;

	public Users selectByPrimaryKey(Long id) {
		return usersDao.selectByPrimaryKey(id);
	}
	public int deleteByPrimaryKey(Long id) {

		return usersDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insertSelective(Users users) {
		int i = 0;
		return usersDao.insertSelective(users);
	}

	@Override
	public int updateByPrimaryKeySelective(Users users) {
		return usersDao.updateByPrimaryKeySelective(users);
	}

	@Override
	public Long selectObjectListPageTotal(Users users) {
		return usersDao.selectObjectListPageTotal(users);
	}

	@Override
	public List<Users> selectObjectListPage(Users users) {
		return usersDao.selectObjectListPage(users);
	}

	@Override
	public List<Users> selectByObjectList(Users users){
		return usersDao.selectByObjectList(users);
	}

}
