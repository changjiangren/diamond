package com.qlzj.diamond.service;

import java.util.List;
import java.util.Date;
import java.util.Map;

import com.qlzj.diamond.common.ApiTemplate;
import com.qlzj.diamond.model.APIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.qlzj.diamond.dao.TCommentReplyDao;
import com.qlzj.diamond.api.ITCommentReplyService;

import com.qlzj.diamond.model.TCommentReply;


/**
 * 
 * 回复表
 * @author menghaojie
 * @date 2018-12-02 21:38
 **/
@Service
public class TCommentReplyService implements ITCommentReplyService {

	private static Logger logger = LoggerFactory.getLogger(TCommentReplyService.class);

	@Resource
	private TCommentReplyDao tCommentReplyDao;

	public TCommentReply selectByPrimaryKey(Long id) {
		return tCommentReplyDao.selectByPrimaryKey(id);
	}
	public int deleteByPrimaryKey(Long id) {

		return tCommentReplyDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insertSelective(TCommentReply tCommentReply) {
		return tCommentReplyDao.insertSelective(tCommentReply);
	}

	@Override
	public int updateByPrimaryKeySelective(TCommentReply tCommentReply) {
		return tCommentReplyDao.updateByPrimaryKeySelective(tCommentReply);
	}

	@Override
	public Long selectObjectListPageTotal(TCommentReply tCommentReply) {
		return tCommentReplyDao.selectObjectListPageTotal(tCommentReply);
	}

	@Override
	public List<TCommentReply> selectObjectListPage(TCommentReply tCommentReply) {
		return tCommentReplyDao.selectObjectListPage(tCommentReply);
	}

	@Override
	public List<TCommentReply> selectByObjectList(TCommentReply tCommentReply){
		return tCommentReplyDao.selectByObjectList(tCommentReply);
	}


	@Override
	public APIResponse<List<TCommentReply>> selectByCommentId(Long commentId){
		return new ApiTemplate<List<TCommentReply>>("comment_reply_select_commentId"){
			@Override
			protected void checkParams() throws Exception {

			}

			@Override
			protected APIResponse<List<TCommentReply>> process() throws Exception {
				List<TCommentReply> tCommentReplyList = tCommentReplyDao.selectByCommentId(commentId);
				return APIResponse.returnSuccess(tCommentReplyList);
			}
		}.execute();
	}


}
