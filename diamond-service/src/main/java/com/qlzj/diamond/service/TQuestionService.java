package com.qlzj.diamond.service;

import java.util.List;

import com.qlzj.diamond.common.ApiTemplate;
import com.qlzj.diamond.common.exception.BizException;
import com.qlzj.diamond.common.utils.JsonUtils;
import com.qlzj.diamond.common.utils.QuestionValiUtil;
import com.qlzj.diamond.model.APIResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.qlzj.diamond.dao.TQuestionDao;
import com.qlzj.diamond.api.ITQuestionService;

import com.qlzj.diamond.model.TQuestion;


/**
 * 
 * 问题表
 * @author menghaojie
 * @date 2018-11-29 17:11
 **/
@Service
public class TQuestionService implements ITQuestionService {

	private static Logger logger = LoggerFactory.getLogger(TQuestionService.class);

	@Resource
	private TQuestionDao tQuestionDao;

	public TQuestion selectByPrimaryKey(Long id) {
		return tQuestionDao.selectByPrimaryKey(id);
	}
	public int deleteByPrimaryKey(Long id) {

		return tQuestionDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insertSelective(TQuestion tQuestion) {
		return tQuestionDao.insertSelective(tQuestion);
	}

	@Override
	public int updateByPrimaryKeySelective(TQuestion tQuestion) {
		return tQuestionDao.updateByPrimaryKeySelective(tQuestion);
	}

	@Override
	public Long selectObjectListPageTotal(TQuestion tQuestion) {
		return tQuestionDao.selectObjectListPageTotal(tQuestion);
	}

	@Override
	public List<TQuestion> selectObjectListPage(TQuestion tQuestion) {
		return tQuestionDao.selectObjectListPage(tQuestion);
	}

	@Override
	public List<TQuestion> selectByObjectList(TQuestion tQuestion){
		return tQuestionDao.selectByObjectList(tQuestion);
	}

	@Override
	public APIResponse publishQuestion(TQuestion question) {
		return new ApiTemplate("question_service_publish"){

			@Override
			protected void checkParams() throws Exception {
				logger.info("tQuestion.publishQuestion 请求参数:{}",JsonUtils.writeObjectAsStringSilently(question));
				if(!QuestionValiUtil.baseValidate(question)){
					throw new BizException(001001,"用户：" + question.getUserId() + " 发表问题异常，参数校验失败！");
				}
			}

			@Override
			protected APIResponse process() throws Exception {
				tQuestionDao.insertSelective(question);
				return APIResponse.returnSuccess();
			}
		}.execute();
	}

	@Override
	public APIResponse modifyQuestion(TQuestion question) {
		return new ApiTemplate("question_service_modify"){

			@Override
			protected void checkParams() throws Exception {
				logger.info("tQuestion 请求参数:{}",JsonUtils.writeObjectAsStringSilently(question));
				//todo 校验参数逻辑
			}

			@Override
			protected APIResponse process() throws Exception {
				tQuestionDao.updateByPrimaryKeySelective(question);
				return APIResponse.returnSuccess();
			}
		}.execute();
	}

	@Override
	public APIResponse deleteQuestionByUserId(Long userId, Long questionId) {
		return new ApiTemplate("question_service_delete"){

			@Override
			protected void checkParams() throws Exception {
				logger.info("用户{}删除问题{}",userId,questionId);
				//todo 校验参数逻辑
			}

			@Override
			protected APIResponse process() throws Exception {
				tQuestionDao.deleteQuestionByUserId(userId,questionId);
				return APIResponse.returnSuccess();
			}
		}.execute();
	}

	@Override
	public APIResponse<List<TQuestion>> searchQuestionByUserId(Long userId) {
		return new ApiTemplate("question_service_search_users_question"){

			@Override
			protected void checkParams() throws Exception {
				logger.info("查询用户{}发布的问题",userId);
				//todo 校验参数逻辑
			}

			@Override
			protected APIResponse process() throws Exception {
				List<TQuestion> questions = tQuestionDao.searchQuestionByUserId(userId);
				return APIResponse.returnSuccess(questions);
			}
		}.execute();
	}

	@Override
	public APIResponse<List<TQuestion>> searchQuestionByLabel(String label) {
		return null;
	}

}
