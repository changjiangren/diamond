package com.qlzj.diamond.service;

import java.util.List;
import java.util.Date;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.qlzj.diamond.dao.TCommentDao;
import com.qlzj.diamond.api.ITCommentService;

import com.qlzj.diamond.model.TComment;


/**
 * 
 * 评论表
 * @author menghaojie
 * @date 2018-12-02 21:38
 **/
@Service
public class TCommentService implements ITCommentService {

	private static Logger logger = LoggerFactory.getLogger(TCommentService.class);

	@Resource
	private TCommentDao tCommentDao;

	public TComment selectByPrimaryKey(Long id) {
		return tCommentDao.selectByPrimaryKey(id);
	}
	public int deleteByPrimaryKey(Long id) {

		return tCommentDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insertSelective(TComment tComment) {
		return tCommentDao.insertSelective(tComment);
	}

	@Override
	public int updateByPrimaryKeySelective(TComment tComment) {
		return tCommentDao.updateByPrimaryKeySelective(tComment);
	}

	@Override
	public Long selectObjectListPageTotal(TComment tComment) {
		return tCommentDao.selectObjectListPageTotal(tComment);
	}

	@Override
	public List<TComment> selectObjectListPage(TComment tComment) {
		return tCommentDao.selectObjectListPage(tComment);
	}

	@Override
	public List<TComment> selectByObjectList(TComment tComment){
		return tCommentDao.selectByObjectList(tComment);
	}

}
