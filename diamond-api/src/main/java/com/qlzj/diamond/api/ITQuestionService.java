package com.qlzj.diamond.api;

import java.util.List;
import java.util.Date;
import java.util.Map;

import com.qlzj.diamond.model.APIResponse;
import com.qlzj.diamond.model.TQuestion;



/**
 * 
 * 问题表
 * @author menghaojie
 * @date 2018-11-29 17:11
 **/
public interface ITQuestionService {

	public TQuestion selectByPrimaryKey(Long id);

	public int deleteByPrimaryKey(Long id);

	public int insertSelective(TQuestion tQuestion);

	public int updateByPrimaryKeySelective(TQuestion tQuestion);

	public Long selectObjectListPageTotal(TQuestion tQuestion);

	public List<TQuestion> selectObjectListPage(TQuestion tQuestion);

	public List<TQuestion> selectByObjectList(TQuestion tQuestion);

	/**
	 * 发布问题
	 * @param question
	 * @return
	 */
	public APIResponse publishQuestion(TQuestion question);

	/**
	 * 修改已发布问题
	 * @param
	 * @return
	 */
	public APIResponse modifyQuestion(TQuestion question);

	/**
	 * 删除已发布问题
	 * @param userId
	 * @param questionId
	 * @return
	 */
	public APIResponse deleteQuestionByUserId(Long userId,Long questionId);

	/**
	 * 查看已发布问题(通过userId)
	 */
	public APIResponse<List<TQuestion>> searchQuestionByUserId(Long userId);

	/**
	 * 查看问题(通过label) 推送这类问题???
	 */
	public APIResponse<List<TQuestion>> searchQuestionByLabel(String label);
}
