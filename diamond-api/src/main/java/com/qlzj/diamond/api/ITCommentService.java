package com.qlzj.diamond.api;

import com.qlzj.diamond.model.TComment;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 
 * 评论表
 * @author menghaojie
 * @date 2018-12-02 21:38
 **/
public interface ITCommentService {

	public TComment selectByPrimaryKey(Long id);

	public int deleteByPrimaryKey(Long id);

	public int insertSelective(TComment tComment);

	public int updateByPrimaryKeySelective(TComment tComment);

	public Long selectObjectListPageTotal(TComment tComment);

	public List<TComment> selectObjectListPage(TComment tComment);

	public List<TComment> selectByObjectList(TComment tComment);

}
