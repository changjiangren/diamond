package com.qlzj.diamond.api;

import java.util.List;
import java.util.Date;
import java.util.Map;

import com.qlzj.diamond.model.Users;


/**
 * 
 * 
 * @author hailin.su
 * @date 2018-11-27 14:11
 **/
public interface IUsersService {

	public Users selectByPrimaryKey(Long id);

	public int deleteByPrimaryKey(Long id);

	public int insertSelective(Users users);

	public int updateByPrimaryKeySelective(Users users);

	public Long selectObjectListPageTotal(Users users);

	public List<Users> selectObjectListPage(Users users);

	public List<Users> selectByObjectList(Users users);

}
