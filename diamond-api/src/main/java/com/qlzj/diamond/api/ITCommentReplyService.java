package com.qlzj.diamond.api;

import java.util.List;
import java.util.Date;
import java.util.Map;

import com.qlzj.diamond.model.APIResponse;
import com.qlzj.diamond.model.TCommentReply;



/**
 * 
 * 回复表
 * @author menghaojie
 * @date 2018-12-02 21:38
 **/
public interface ITCommentReplyService {

	public TCommentReply selectByPrimaryKey(Long id);

	public int deleteByPrimaryKey(Long id);

	public int insertSelective(TCommentReply tCommentReply);

	public int updateByPrimaryKeySelective(TCommentReply tCommentReply);

	public Long selectObjectListPageTotal(TCommentReply tCommentReply);

	public List<TCommentReply> selectObjectListPage(TCommentReply tCommentReply);

	public List<TCommentReply> selectByObjectList(TCommentReply tCommentReply);

	public APIResponse<List<TCommentReply>> selectByCommentId(Long commentId);

}
