package com.qlzj.diamond.controller.question;

import com.qlzj.diamond.common.utils.JsonUtils;
import com.qlzj.diamond.model.APIResponse;
import com.qlzj.diamond.model.TCommentReply;
import com.qlzj.diamond.model.TQuestion;
import com.qlzj.diamond.service.TCommentReplyService;
import com.qlzj.diamond.service.TQuestionService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: haojiemeng
 * @Description: java类作用描述
 * @CreateDate: 2018/12/2 21:43
 */

@RestController
@RequestMapping("/qlzi/commentReply/")
public class CommentReplyController {

    @Resource
    private TCommentReplyService tCommentReplyService;
    @RequestMapping("reply")
    public APIResponse commentReply(@RequestBody TCommentReply commentReply){
         tCommentReplyService.insertSelective(commentReply);
        return APIResponse.returnSuccess();
    }

    @RequestMapping("search")
    public APIResponse<List<TCommentReply>> getQuestionListByUserId(Long commentId){
        return  tCommentReplyService.selectByCommentId(commentId);
    }

/*    @RequestMapping("modify")
    public APIResponse modifyQuestionByUserId(@RequestBody TQuestion question){
        tQuestionService.updateByPrimaryKeySelective(question);
        return APIResponse.returnSuccess();
    }*/

    @RequestMapping("delete")
    public APIResponse deleteCommentReplyById(Long id){
        tCommentReplyService.deleteByPrimaryKey(id);
        return APIResponse.returnSuccess();
    }




    public static void main(String[] args) {
        TCommentReply commentReply = new TCommentReply();
        commentReply.setReplayContent("你好");
        commentReply.setReplyuserId(1000l);
        commentReply.setUserId(2000l);
        commentReply.setReplyuserName("浩杰");
        commentReply.setUserName("杨兆");
        commentReply.setCommentId(1l);
        System.out.println(JsonUtils.writeObjectAsStringSilently(commentReply));
    }

}
