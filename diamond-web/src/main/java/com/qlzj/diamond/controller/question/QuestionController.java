package com.qlzj.diamond.controller.question;

import com.qlzj.diamond.common.utils.JsonUtils;
import com.qlzj.diamond.model.APIResponse;
import com.qlzj.diamond.model.TQuestion;
import com.qlzj.diamond.service.TQuestionService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author haojiem@tujia.com
 * @date 2018/11/29
 */
@RestController
@RequestMapping("/qlzi/question/")
public class QuestionController {

    @Resource
    private TQuestionService tQuestionService;
    @RequestMapping("publish")
    public APIResponse publishedQuestion(@RequestBody TQuestion question){
        return tQuestionService.publishQuestion(question);
    }

    @RequestMapping("search")
    public APIResponse<List<TQuestion>> getQuestionListByUserId(Long userId){
        return tQuestionService.searchQuestionByUserId(userId);
    }

    @RequestMapping("modify")
    public APIResponse modifyQuestionByUserId(@RequestBody TQuestion question){
         tQuestionService.updateByPrimaryKeySelective(question);
         return APIResponse.returnSuccess();
    }

    @RequestMapping("delete")
    public APIResponse deleteQuestionByUserId(Long userId,Long questionId){
        tQuestionService.deleteQuestionByUserId(userId,questionId);
        return APIResponse.returnSuccess();
    }




    public static void main(String[] args) {
        TQuestion question = new TQuestion();
        question.setQuesContent("程序猿的工作与生活");
        question.setQuesTitle("1、对项目经理负责，负责软件项目的详细设计、编码和内部测试的组织实施，对\n" +
                "程序员小型软件项目兼任系统分析工作，完成分配项目的实施和技术支持工作。\n" +
                "2、协助项目经理和相关人员同客户进行沟通，保持良好的客户关系。\n" +
                "3、参与需求调研、项目可行性分析、技术可行性分析和需求分析。\n" +
                "4、熟悉并熟练掌握交付软件部开发的软件项目的相关软件技术。\n" +
                "5、负责向项目经理及时反馈软件开发中的情况，并根据实际情况提出改进建议。\n" +
                "6、参与软件开发和维护过程中重大技术问题的解决，参与软件首次安装调试、数据割接、用户培训和项目推广。\n" +
                "7、负责相关技术文档的拟订。\n" +
                "8、负责对业务领域内的技术发展动态。");
        question.setUserId(10000l);
        System.out.println(JsonUtils.writeObjectAsStringSilently(question));
    }

}
