package com.qlzj.diamond.controller.question;

import com.qlzj.diamond.api.ITCommentService;
import com.qlzj.diamond.model.APIResponse;
import com.qlzj.diamond.model.TComment;
import com.qlzj.diamond.model.TQuestion;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author haojiem@tujia.com
 * @date 2018/12/2
 */
@RestController
@RequestMapping("/qlzj/comment")
public class CommentController {
    
    @Resource
    private ITCommentService commentService;

    @RequestMapping("add")
    public APIResponse publishedQuestion(@RequestBody TComment comment){
         commentService.insertSelective(comment);
         return APIResponse.returnSuccess();
    }

    @RequestMapping("search")
    public APIResponse<List<TComment>> searchComment(TComment comment){
        List<TComment> comments = commentService.selectByObjectList(comment);
        return APIResponse.returnSuccess(comments);
    }

    @RequestMapping("delete")
    public APIResponse deleteQuestionByUserId(Long commentId){
       /* commentService.(userId,questionId);
        return APIResponse.returnSuccess();*/
       return null;
    }
    
}
