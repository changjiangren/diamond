package com.qlzj.diamond.controller;

import com.qlzj.diamond.api.IUsersService;
import com.qlzj.diamond.model.Users;
import com.qlzj.diamond.security.social.AppSingUpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.ServletWebRequest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author haojiem@tujia.com
 * @date 2018/11/27
 */
@RestController
public class TestController {

    @Resource
    private IUsersService usersService;

    @Autowired
	private AppSingUpUtils appSingUpUtils;

    @GetMapping("/me")
    Object getCurrentUser(@AuthenticationPrincipal UserDetails user){
        String result = "当前没有登录！";
        if(user == null){
            return result;
        }
        return user;
    }

    @PostMapping("/user/regist")
    public void regist(Users user, HttpServletRequest request) {
        //不管是注册用户还是绑定用户，都会拿到一个用户唯一标识。
        String userId = user.getPhone();
		appSingUpUtils.doPostSignUp(new ServletWebRequest(request), userId);
    }

    @GetMapping("/authentication")
    Object getCurrentUser(Authentication authentication){
        return authentication;
    }

    @RequestMapping("/select")
    @ResponseBody
    Users select(Long userId){
        return usersService.selectByPrimaryKey(userId);
    }

    @RequestMapping("/add")
    @ResponseBody
    int add(@RequestBody Users users){
        return usersService.insertSelective(users);
    }

    @RequestMapping("/update")
    @ResponseBody
    int update(@RequestBody Users users){
        return usersService.updateByPrimaryKeySelective(users);
    }

    @RequestMapping("/delete")
    @ResponseBody
    int delete(@RequestBody Users users){
        return usersService.deleteByPrimaryKey(users.getID());
    }
}
