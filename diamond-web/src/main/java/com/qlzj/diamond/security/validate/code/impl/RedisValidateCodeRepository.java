/**
 * 
 */
package com.qlzj.diamond.security.validate.code.impl;

import com.qlzj.diamond.common.security.validate.ValidateCode;
import com.qlzj.diamond.common.security.validate.ValidateCodeException;
import com.qlzj.diamond.common.security.validate.ValidateCodeRepository;
import com.qlzj.diamond.common.security.validate.ValidateCodeType;
import com.qlzj.diamond.common.utils.RedisUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * 基于redis的验证码存取器，避免由于没有session导致无法存取验证码的问题
 */
@Component
public class RedisValidateCodeRepository implements ValidateCodeRepository {

//	@Autowired
//	private RedisTemplate<Object, Object> redisTemplate;

	@Autowired
	private RedisUtil redisUtil;

	@Override
	public void save(ServletWebRequest request, ValidateCode code, ValidateCodeType type) {
		redisUtil.set(buildKey(request, type), code, 1800);
	}

	@Override
	public ValidateCode get(ServletWebRequest request, ValidateCodeType type) {
		Object value = redisUtil.get(buildKey(request, type));
		if (value == null) {
			return null;
		}
		return (ValidateCode) value;
	}

	@Override
	public void remove(ServletWebRequest request, ValidateCodeType type) {
		redisUtil.del(buildKey(request, type));
	}

	/**
	 * @param request
	 * @param type
	 * @return
	 */
	private String buildKey(ServletWebRequest request, ValidateCodeType type) {
		String deviceId = request.getHeader("deviceId");
		if (StringUtils.isBlank(deviceId)) {
			throw new ValidateCodeException("请在请求头中携带deviceId参数");
		}
		return "code:" + type.toString().toLowerCase() + ":" + deviceId;
	}

}
