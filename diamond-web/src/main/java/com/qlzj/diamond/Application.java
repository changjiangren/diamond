package com.qlzj.diamond;

/**
 * @author haojiem@tujia.com
 * @date 2018/11/24
 */

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 应用启动类
 */
@SpringBootApplication
@MapperScan("com.qlzj.diamond.dao")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
        System.out.println("SpringBoot Started");
    }
}
