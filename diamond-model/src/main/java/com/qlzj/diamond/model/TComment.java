package com.qlzj.diamond.model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 
 * 评论表
 * @author menghaojie
 * @date 2018-12-02 21:38
 **/
public class TComment implements Serializable {

	private static final long serialVersionUID = 4532859710916455442L;

	/****/
	private Long id;

	/**问题ID**/
	private Long questionId;

	/**用户ID**/
	private Long userId;

	/**评论内容**/
	private String answerContent;

	/**点赞数量**/
	private Integer praiseCount;

	/**审核状态: 0 待审核 1 通过 2 不通过**/
	private Integer auditStatus;

	/**创建时间**/
	private Date createTime;

	/**更新时间**/
	private Date updateTime;



	public void setId(Long id){
		this.id = id;
	}

	public Long getId(){
		return this.id;
	}

	public void setQuestionId(Long questionId){
		this.questionId = questionId;
	}

	public Long getQuestionId(){
		return this.questionId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setAnswerContent(String answerContent){
		this.answerContent = answerContent;
	}

	public String getAnswerContent(){
		return this.answerContent;
	}

	public void setPraiseCount(Integer praiseCount){
		this.praiseCount = praiseCount;
	}

	public Integer getPraiseCount(){
		return this.praiseCount;
	}

	public void setAuditStatus(Integer auditStatus){
		this.auditStatus = auditStatus;
	}

	public Integer getAuditStatus(){
		return this.auditStatus;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	@Override
	public String toString() {
		return "TComment [ id= "+id+
			",questionId= "+questionId+
			",userId= "+userId+
			",answerContent= "+answerContent+
			",praiseCount= "+praiseCount+
			",auditStatus= "+auditStatus+
			",createTime= "+createTime+
			",updateTime= "+updateTime+"]";
	}
}
