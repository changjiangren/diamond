package com.qlzj.diamond.model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 
 * 回复表
 * @author menghaojie
 * @date 2018-12-02 21:38
 **/
public class TCommentReply implements Serializable {

	private static final long serialVersionUID = 6278617145266985809L;

	/****/
	private Long id;

	/**评论id**/
	private Long commentId;

	/**用户id**/
	private Long userId;

	/**用户名**/
	private String userName;

	/**被回复的用户id**/
	private Long replyuserId;

	/**被回复的用户名**/
	private String replyuserName;

	/**回复内容**/
	private String replayContent;

	/**审核状态: 0 待审核 1 通过 2 不通过**/
	private Integer auditStatus;

	/**创建时间**/
	private Date createTime;

	/**更新时间**/
	private Date updateTime;



	public void setId(Long id){
		this.id = id;
	}

	public Long getId(){
		return this.id;
	}

	public void setCommentId(Long commentId){
		this.commentId = commentId;
	}

	public Long getCommentId(){
		return this.commentId;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return this.userName;
	}

	public void setReplyuserId(Long replyuserId){
		this.replyuserId = replyuserId;
	}

	public Long getReplyuserId(){
		return this.replyuserId;
	}

	public void setReplyuserName(String replyuserName){
		this.replyuserName = replyuserName;
	}

	public String getReplyuserName(){
		return this.replyuserName;
	}

	public void setReplayContent(String replayContent){
		this.replayContent = replayContent;
	}

	public String getReplayContent(){
		return this.replayContent;
	}

	public void setAuditStatus(Integer auditStatus){
		this.auditStatus = auditStatus;
	}

	public Integer getAuditStatus(){
		return this.auditStatus;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	@Override
	public String toString() {
		return "TCommentReply [ id= "+id+
			",commentId= "+commentId+
			",userId= "+userId+
			",userName= "+userName+
			",replyuserId= "+replyuserId+
			",replyuserName= "+replyuserName+
			",replayContent= "+replayContent+
			",auditStatus= "+auditStatus+
			",createTime= "+createTime+
			",updateTime= "+updateTime+"]";
	}
}
