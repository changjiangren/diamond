package com.qlzj.diamond.model.recommend;

public class SimilarytyArrays {

    private Long targetId;

    private long[] userInterest;

    private long[] targetInterest;

    private long[] userWant;

    private long[] targetCan;

    public Long getTargetId() {
        return targetId;
    }

    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    public long[] getUserInterest() {
        return userInterest;
    }

    public void setUserInterest(long[] userInterest) {
        this.userInterest = userInterest;
    }

    public long[] getTargetInterest() {
        return targetInterest;
    }

    public void setTargetInterest(long[] targetInterest) {
        this.targetInterest = targetInterest;
    }

    public long[] getUserWant() {
        return userWant;
    }

    public void setUserWant(long[] userWant) {
        this.userWant = userWant;
    }

    public long[] getTargetCan() {
        return targetCan;
    }

    public void setTargetCan(long[] targetCan) {
        this.targetCan = targetCan;
    }
}
