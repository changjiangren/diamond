package com.qlzj.diamond.model.recommend;

public class Recommend implements Comparable<Recommend> {

    private Double distance;

    private Long id;

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int compareTo(Recommend recommend) {
        return recommend.distance.compareTo(this.distance);
    }

    @Override
    public String toString() {
        return "Recommend{" +
                "distance=" + distance +
                ", id=" + id +
                '}';
    }
}
