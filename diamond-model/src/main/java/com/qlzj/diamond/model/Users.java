package com.qlzj.diamond.model;

import java.io.Serializable;


/**
 * 
 * 
 * @author hailin.su
 * @date 2018-11-27 14:11
 **/
public class Users implements Serializable {

	private static final long serialVersionUID = 881497403101697072L;

	/**id**/
	private Long ID;

	/****/
	private Integer Type;

	/**手机**/
	private String phone;



	public void setID(Long ID){
		this.ID = ID;
	}

	public Long getID(){
		return this.ID;
	}

	public void setType(Integer Type){
		this.Type = Type;
	}

	public Integer getType(){
		return this.Type;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return this.phone;
	}

	@Override
	public String toString() {
		return "Users [ ID= "+ID+
			",Type= "+Type+
			",phone= "+phone+"]";
	}
}
