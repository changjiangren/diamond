package com.qlzj.diamond.model;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 
 * 问题表
 * @author menghaojie
 * @date 2018-11-29 17:11
 **/
public class TQuestion implements Serializable {

	private static final long serialVersionUID = 3889359059114073862L;

	/**ID**/
	private Long id;

	/**用户ID**/
	private Long userId;

	/**问题标题**/
	private String quesTitle;

	/**问题内容**/
	private String quesContent;

	/**问题标签（逗号分隔）**/
	private String quesLable;

	/**评论数量**/
	private Integer commentCount;

	/**点赞数量**/
	private Integer praiseCount;

	/**热度值**/
	private Integer hotValue;

	/**匿名状态: 0 非匿名 1 匿名**/
	private Integer anonymousStatus;

	/**公开状态: 0 不公开 1 公开 2 好友可看**/
	private Integer publicStatus;

	/**创建时间**/
	private Date createTime;

	/**更新时间**/
	private Date updateTime;

	public void setId(Long id){
		this.id = id;
	}

	public Long getId(){
		return this.id;
	}

	public void setUserId(Long userId){
		this.userId = userId;
	}

	public Long getUserId(){
		return this.userId;
	}

	public void setQuesTitle(String quesTitle){
		this.quesTitle = quesTitle;
	}

	public String getQuesTitle(){
		return this.quesTitle;
	}

	public void setQuesContent(String quesContent){
		this.quesContent = quesContent;
	}

	public String getQuesContent(){
		return this.quesContent;
	}

	public void setQuesLable(String quesLable){
		this.quesLable = quesLable;
	}

	public String getQuesLable(){
		return this.quesLable;
	}

	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}

	public void setPraiseCount(Integer praiseCount){
		this.praiseCount = praiseCount;
	}

	public Integer getPraiseCount(){
		return this.praiseCount;
	}

	public void setHotValue(Integer hotValue){
		this.hotValue = hotValue;
	}

	public Integer getHotValue(){
		return this.hotValue;
	}

	public Integer getAnonymousStatus() {
		return anonymousStatus;
	}

	public void setAnonymousStatus(Integer anonymousStatus) {
		this.anonymousStatus = anonymousStatus;
	}

	public void setPublicStatus(Integer publicStatus){
		this.publicStatus = publicStatus;
	}

	public Integer getPublicStatus(){
		return this.publicStatus;
	}

	public void setCreateTime(Date createTime){
		this.createTime = createTime;
	}

	public Date getCreateTime(){
		return this.createTime;
	}

	public void setUpdateTime(Date updateTime){
		this.updateTime = updateTime;
	}

	public Date getUpdateTime(){
		return this.updateTime;
	}

	@Override
	public String toString() {
		return "TQuestion [ id= "+id+
			",userId= "+userId+
			",quesTitle= "+quesTitle+
			",quesContent= "+quesContent+
			",quesLable= "+quesLable+
			",praiseCount= "+praiseCount+
			",hotValue= "+hotValue+
			",anonymousStatus= "+anonymousStatus+
			",publicStatus= "+publicStatus+
			",createTime= "+createTime+
			",updateTime= "+updateTime+"]";
	}
}
